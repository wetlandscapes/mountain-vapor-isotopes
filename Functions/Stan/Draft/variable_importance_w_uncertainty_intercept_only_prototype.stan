data {
  int<lower=1> N;        // number of observations
  vector[N] atmos_obs;   // response variable
  vector[N] atmos_equil; //Expected equilibrium value based on one of the methods
  vector[N] chi;         //The mixing ratio (ppm) measured at same time as atmos_obs
  vector[2] b0_p;        //Prior for the intercept of the measurement uncertainty
                         //b0_p[1]: mean
                         //b0_p[2]: standard deviation                  
  vector[2] b1_p;        //Prior for the slope of the measurement uncertainty
                         //b1_p[1]: mean
                         //b1_p[2]: standard deviation
  real<lower = 0> sig; //Spread in the mean vapor value
}

parameters {
  vector[N] atmos_true; //Estimation of the "true" vapor signal
  real intercept;
  real<lower=0> b0;
  real<lower=0> b1;
  real<lower = 0> sigma; //Spread in the mean vapor value
}

model {
  //Uncertainties in the observed data
  b0 ~ normal(b0_p[1], b0_p[2]);
  b1 ~ normal(b1_p[1], b1_p[2]);
  atmos_obs ~ normal(atmos_true, b0 + b1 ./ chi);
  //Likelihood
  atmos_true ~ normal(intercept + atmos_equil, sigma);
}

generated quantities {
  real loglik[N];
  real posterior[N];
  for(n in 1:N){
    loglik[n] = normal_lpdf(atmos_obs | intercept + atmos_equil[n], sigma);
  }
  for(n in 1:N){
    posterior[n] = normal_rng(intercept + atmos_equil[n], sigma);
  }
}
