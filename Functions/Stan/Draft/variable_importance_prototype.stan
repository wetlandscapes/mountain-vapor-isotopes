data {
  int<lower=1> N;  // number of observations
  int<lower=1> M; //number of covariates
  vector[N] Y;  // response variable
  matrix[N, M] X;
}

transformed data {
  //initialize
  matrix[N, M+1] X_int;
  vector[N] const_int;
  
  //make
  for(n in 1:N) {
    const_int[n] = 1;
  }
  
  //transform
  X_int = append_col(const_int, X);
}

parameters {
  vector[M+1] b;
  real<lower=0> sigma;
}

model {
  Y ~ normal(X_int * b, sigma);
}
