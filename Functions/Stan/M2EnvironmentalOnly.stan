data {
  int<lower = 0> N;
  int<lower = 0> P;
  matrix[N, P] enviro;
  vector[N] atmos;
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
}

parameters {
  vector[P] beta;
  real<lower = 0> sigma;
  real alpha;
}

model {
  sigma ~ cauchy(0, sig);
  alpha ~ normal(0, 100);
  beta ~ normal(0, 5);
  atmos ~ normal(alpha + enviro * beta, sigma);
}
