data {
  int<lower = 0> N;
  vector[N] O18;
  vector[N] H2;
  real sig; /*Family/likelihood variance. Prior for the "variance" term in the 
  half-cauchy - needed for the different isotopes, as they are inherently more
  and less noisy.*/
  real mK; //Shape value for the slope: Weibull.
  real mLambda; //Scale value for the slope: Weibull.
  real bMu; //Mean value for the intercept: Normal.
  real bSigma; //Variance term for the intercept: Normal.
}

parameters {
  real<lower = 0> sigma;
  real<lower = 0> m;
  real b;
}

model {
  target += cauchy_lpdf(sigma | 0, sig);
  //sigma ~ cauchy(0, sig);
  target += weibull_lpdf(m | mK, mLambda);
  //m ~ weibull(mK, mLambda);
  target += normal_lpdf(b | bMu, bSigma);
  //b ~ normal(bMu, bSigma);
  target += normal_lpdf(H2 | m * O18 + b, sigma);
  //H2 ~ normal(m * O18 + b, sigma);
}

/*generated quantities {
 vector[N] log_lik;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(H2[n] | m * O18[n] + b, sigma);
  }
}*/
