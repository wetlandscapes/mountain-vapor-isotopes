data {
  int<lower = 0> N;
  int<lower = 0> P;
  vector[N] equil;
  matrix[N, P] enviro;
  vector[N] atmos;
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
}

parameters {
  vector[P] beta;
  real<lower = 0> sigma;
  real<lower = 0> hyperSig;
}

model {
  hyperSig ~ normal(0, 200);
  sigma ~ cauchy(0, sig);
  beta ~ normal(0, hyperSig);
  atmos ~ normal(equil + enviro * beta, sigma);
}

generated quantities {
  vector[N] log_lik;
  vector[N] pred;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(atmos[n] | equil[n] + enviro[n] * beta, sigma);
  }
  for (n in 1:N) {
    pred[n] = normal_rng(equil[n] + enviro[n] * beta, sigma);
  }
}
