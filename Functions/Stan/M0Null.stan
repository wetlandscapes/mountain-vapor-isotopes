data {
  int<lower = 0> N;
  vector[N] atmos;
  real ave; //Prior for the average observed data.
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
}

parameters {
  real mu;
  real<lower = 0> sigma;
}

model {
  mu ~ cauchy(ave, sig);
  sigma ~ cauchy(0, sig);
  atmos ~ normal(mu, sigma);
}

generated quantities {
  vector[N] log_lik;
  vector[N] pred;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(atmos[n] | mu, sigma);
  }
  for (n in 1:N) {
    pred[n] = normal_rng(mu, sigma);
  }
}
