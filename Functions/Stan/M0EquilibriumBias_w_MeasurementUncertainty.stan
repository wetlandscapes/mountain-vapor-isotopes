data {
  int<lower = 0> N; //Number of atmospheric observations
  vector[N] atmos_obs; //The measured atmospheric vapor value from the LGR
  vector[N] atmos_equil; //Expected equilibrium value based on one of the methods
  vector[N] chi; //The mixing ratio (ppm) measured at same time as atmos_obs
  vector[2] b0_p; //Prior for the intercept of the measurement uncertainty
                  //b0_p[1]: mean
                  //b0_p[2]: standard deviation                  
  vector[2] b1_p; //Prior for the slope of the measurement uncertainty
                  //b1_p[1]: mean
                  //b1_p[2]: standard deviation
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
  real diseq; //Prior for the disequilibrium term.
}

transformed data {
  //Using an inverted form of vapor concetration seems to generate more stable
  //parameter estimates.
  vector[N] chi_inv;
  chi_inv = 1 ./ chi; //Vectorized division
}

parameters {
  vector[N] atmos_true; //Estimation of the "true" vapor signal
  real<lower = 0> b0; //Same conditions as the heteroskedastic model
  real<lower = 0> b1; //Same...
  real<lower = 0> sigma;
  real bias;
}

model {
  //Uncertainties in the observed data
  b0 ~ normal(b0_p[1], b0_p[2]);
  b1 ~ normal(b1_p[1], b1_p[2]);
  
  //Global parameters
  sigma ~ cauchy(0, sig);
  bias ~ normal(diseq, sig);
  
  //Estimate the "true" atmospheric value based on heterskedastic uncertainties
  atmos_obs ~ normal(atmos_true, b0 + b1 * chi_inv);
  
  //Apply model using the distribution of "true" vapor values
    //I've put these last two lines out of order in some of the other models,
    // but I think it's better to just leave things as-is for now
  atmos_true ~ normal(atmos_equil - bias, sigma);
}

generated quantities {
  vector[N] log_lik;
  vector[N] pred;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(atmos_obs[n] | atmos_equil[n] - bias, sigma);
  }
  for (n in 1:N) {
    pred[n] = normal_rng(atmos_equil[n] - bias, sigma);
  }
}
