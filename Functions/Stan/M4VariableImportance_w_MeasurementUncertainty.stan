data {
  int<lower=1> N;  //Number of atmospheric observations
  int<lower=1> M; //Number of Covariates
  vector[N] atmos_obs; //The measured atmospheric vapor value from the LGR
  vector[N] atmos_equil; //Expected equilibrium value based on one of the methods
  matrix[N, M] X; //The design matrix, without an intercept column
  vector[N] chi; //The mixing ratio (ppm) measured at same time as atmos_obs
  vector[2] b0_p; //Prior for the intercept of the measurement uncertainty
                  //b0_p[1]: mean
                  //b0_p[2]: standard deviation                  
  vector[2] b1_p; //Prior for the slope of the measurement uncertainty
                  //b1_p[1]: mean
                  //b1_p[2]: standard deviation
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
}

transformed data {
  //initialize
  matrix[N, M+1] X_int;
  vector[N] const_int;
  
  //Add intercept column to design matrix
  //make
  for(n in 1:N) {
    const_int[n] = 1;
  }
  //transform to include intercept
  X_int = append_col(const_int, X);
}

parameters {
  vector[N] atmos_true; //Estimation of the "true" vapor signal
  vector[M+1] b;
  real<lower=0> b0;
  real<lower=0> b1;
  real<lower = 0> sigma; //Spread in the mean vapor value
}

model {
  vector[N] atmos_apparent;
  //Uncertainties in the observed data
  b0 ~ normal(b0_p[1], b0_p[2]);
  b1 ~ normal(b1_p[1], b1_p[2]);
  
  sigma ~ cauchy(0, sig);
  atmos_obs ~ normal(atmos_true, b0 + b1 ./ chi);
  atmos_apparent = X_int * b;
  //Likelihood
    //Subtracting to be consistent with apparent disequilibrium term
  atmos_true ~ normal(atmos_equil - atmos_apparent, sigma);
}

generated quantities {
  real loglik[N];
  real posterior[N];
  vector[N] atmos_apparent;
  atmos_apparent = X_int * b;
  for(n in 1:N){
    loglik[n] = normal_lpdf(atmos_obs | atmos_equil[n] - atmos_apparent[n], sigma);
  }
  for(n in 1:N){
    posterior[n] = normal_rng(atmos_equil[n] - atmos_apparent[n], sigma);
  }
}

