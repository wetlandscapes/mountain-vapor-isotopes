/*
This model was used to examine the heteroscedastic uncertainty in the
relationship between the measured and observed (from a standard) vapor values.
*/
data {
  int<lower = 0> N;
  vector[N] obs; //the "true" isotopic value
  vector[N] pred; //measured isotopic value
  vector[N] chi; //uncertainty covariate; vapor concentration (ppm)
}

transformed data {
  //Using an inverted form of vapor concetration seems to generate more stable
  //parameter estimates.
  vector[N] chi_inv;
  
  for(n in 1:N){
    chi_inv[n] = 1 / chi[n];
  }
}

parameters {
  real<lower=0> b0; //intercept of uncertainty relationship
  real<lower=0> b1; //uncertainty slope
  //real<lower=0> sigma;
}

model {
  //priors
  b0 ~ cauchy(0, 1);
  b1 ~ cauchy(0, 100);
  //sigma ~ cauchy(0, 10);
  
  //relationship
  obs ~ normal(pred, b0 + b1 * chi_inv);
  //obs ~ normal(pred, sigma * chi_inv);
}

generated quantities {
  //initialize outputs
  vector[N] log_lik;
  vector[N] posterior;
  
  //execute
  //log likelihood estimates
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(obs[n] | pred[n], b0 + b1 * chi_inv[n]);
    //log_lik[n] = normal_lpdf(obs[n] | pred[n], sigma * chi_inv[n]);
  }
  
  //posterior estimates
  for (n in 1:N) {
    posterior[n] = normal_rng(pred[n], b0 + b1 * chi_inv[n]);
    //posterior[n] = normal_rng(pred[n], sigma * chi_inv[n]);
  }
}
