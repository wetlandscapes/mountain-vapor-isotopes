/*
Model used to incorporate measurement uncertainties (MeasurementError.stan) from
the measured atmospheric vapor values, in the context of the equilibrium
assumption.
*/
data {
  int<lower = 0> N;
  vector[N] equil; //Data: Estimated vapor isotope value from the equilibrium
  //assumption (permil)
  vector[N] atmos; //Data: Vapor isotope value (permil)
  vector[N] chi; //Data: Vapor concentration (ppm)
  real b0_mu; //Prior: heteroscedasitc intercept from MeasurementError.stan
  //real b0_sd;
  real b1_mu; //Prior: heteroscedasitc slope from MeasurementError.stan
  //real b1_sd;
  real sig; //Prior: overall uncertainty in the relationship
}

transformed data{
  //Using an inverted form of vapor concetration seems to generate more stable
  //parameter estimates.
  vector[N] chi_inv;
  
  for(n in 1:N){
    chi_inv[n] = 1 / chi[n];
  }
}

parameters {
  real<lower = 0> sigma_m;
  real<lower = 0> sigma_a;
  //real<lower = 0> b0;
  //real<lower = 0> b1;
}

/*
transformed parameters {
  real b0;
  real b1;
  vector[N] sigma_m;
  sigma_m = b0 + b1 * chi_inv;
}
*/

model {
  //b0 ~ normal(b0_mu, b0_sd);
  //b1 ~ normal(b1_mu, b1_sd);
  sigma_m ~ cauchy(0, b0_mu + b1_mu * chi_inv);
  sigma_a ~ cauchy(0, sig);
  
  //Note the recursion. This is done to account for the measurement uncertainty
  // in `atmos`. For more on how this works, see section 14.1.1 of McElreath's
  // "Statistical Rethinking" book.
  atmos ~ normal(equil, sigma_a);
  equil ~ normal(atmos, sigma_m);
}


generated quantities {
  vector[N] log_lik; //Used for comparison of models
  vector[N] posterior; //Posterior prediction
  
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(atmos[n] | equil[n], sig);
  }
  
  for (n in 1:N) {
    posterior[n] = normal_rng(equil[n], sig);
  }
}
