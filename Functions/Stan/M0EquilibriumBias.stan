data {
  int<lower = 0> N;
  vector[N] equil;
  vector[N] atmos;
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
  real diseq; //Prior for the disequilibrium term.
}

parameters {
  real<lower = 0> sigma;
  real bias;
}

model {
  sigma ~ cauchy(0, sig);
  bias ~ normal(diseq, sig);
  atmos ~ normal(equil - bias, sigma);
}

generated quantities {
  vector[N] log_lik;
  vector[N] pred;
  for (n in 1:N) {
    log_lik[n] = normal_lpdf(atmos[n] | equil[n] - bias, sigma);
  }
  for (n in 1:N) {
    pred[n] = normal_rng(equil[n] - bias, sigma);
  }
}
