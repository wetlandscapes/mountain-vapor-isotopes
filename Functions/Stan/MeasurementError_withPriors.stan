/*
This model was used to examine the heteroscedastic uncertainty in the
relationship between the measured and observed (from a standard) vapor values.
*/
data {
  int<lower = 0> N;
  vector[N] obs; //the "true" isotopic value (permille)
  vector[N] pred; //the predicted isotopic value, after correction (permille)
  vector[N] chi; //uncertainty covariate; vapor concentration (ppm)
  vector[2] b0_p; //priors for the intercept of the uncertainty relationship
               //(permille)
  real b1_p;  //priors for the slope (permille / ppm) 
}

transformed data {
  //Using an inverted form of vapor concetration seems to generate more stable
  //parameter estimates.
  vector[N] chi_inv;
  
  chi_inv = 1 ./ chi; //Vectorized division
}

parameters {
  real<lower=0> b0; //intercept of uncertainty relationship
  real<lower=0> b1; //uncertainty slope
}

model {
  //priors
  b0 ~ normal(b0_p[1], b0_p[2]); //priors from the literature
  b1 ~ cauchy(b1_p, 100); //using an uniformative prior
  
  //relationship
  obs ~ normal(pred, b0 + b1 * chi_inv);
}

generated quantities {
  //initialize outputs
  vector[N] posterior;
  
  //posterior estimates
  for (n in 1:N) {
    posterior[n] = normal_rng(pred[n], b0 + b1 * chi_inv[n]);
    //posterior[n] = normal_rng(pred[n], sigma * chi_inv[n]);
  }
}
