data {
  int<lower = 0> N;
  vector[N] equil;
  vector[N] atmos;
  real sig; /*Prior for the "variance" term in the half-cauchy - needed for the
  different isotopes, as they are inherently more and less noisy.*/
}

parameters {
  real alpha;
  real beta;
  real<lower = 0> sigma;
}

model {
  sigma ~ cauchy(0, sig);
  atmos ~ normal(alpha + beta * equil, sigma);
}
