clear;
load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2014A2_v1_2_CalCoeffs.mat')
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = X_v;
O18_ALL = O18;
D_ALL = D;
O18_Fit_ALL = O18_Fit;
D_Fit_ALL = D_Fit;
O18_Residual_ALL = O18_Residual;
D_Residual_ALL = D_Residual;

load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2014B_v1_2_CalCoeffs.mat')
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = [X_v_ALL X_v];
O18_ALL = [O18_ALL O18];
D_ALL = [D_ALL D];
O18_Fit_ALL = [O18_Fit_ALL O18_Fit];
D_Fit_ALL = [D_Fit_ALL D_Fit];
O18_Residual_ALL = [O18_Residual_ALL O18_Residual];
D_Residual_ALL = [D_Residual_ALL D_Residual];

load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2014C_v1_2_CalCoeffs.mat')
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = [X_v_ALL X_v];
O18_ALL = [O18_ALL O18];
D_ALL = [D_ALL D];
O18_Fit_ALL = [O18_Fit_ALL O18_Fit];
D_Fit_ALL = [D_Fit_ALL D_Fit];
O18_Residual_ALL = [O18_Residual_ALL O18_Residual];
D_Residual_ALL = [D_Residual_ALL D_Residual];

load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2014D_v1_2_CalCoeffs.mat')
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = [X_v_ALL X_v];
O18_ALL = [O18_ALL O18];
D_ALL = [D_ALL D];
O18_Fit_ALL = [O18_Fit_ALL O18_Fit];
D_Fit_ALL = [D_Fit_ALL D_Fit];
O18_Residual_ALL = [O18_Residual_ALL O18_Residual];
D_Residual_ALL = [D_Residual_ALL D_Residual];

load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2015A_v1_2_CalCoeffs.mat')
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = [X_v_ALL X_v];
O18_ALL = [O18_ALL O18];
D_ALL = [D_ALL D];
O18_Fit_ALL = [O18_Fit_ALL O18_Fit];
D_Fit_ALL = [D_Fit_ALL D_Fit];
O18_Residual_ALL = [O18_Residual_ALL O18_Residual];
D_Residual_ALL = [D_Residual_ALL D_Residual];

load('F:\AmeriFlux - New\Data\data - LGR\Version 1.2\LGR_2015A_v1_2v2_CalCoeffs.mat');
X_v = CalCoeffs.CalData.X_v;
D = CalCoeffs.CalData.D;
O18 = CalCoeffs.CalData.O18;
CalStandard_O18 = CalCoeffs.CalData.CalStandard_O18;
CalStandard_D = CalCoeffs.CalData.CalStandard_D;
O18_Fit = ((CalStandard_O18+1000)./feval(CalCoeffs.p_O18,X_v)-1000)';
D_Fit = ((CalStandard_D+1000)./feval(CalCoeffs.p_D,X_v)-1000)';
O18_Residual = O18 - O18_Fit;
D_Residual = D - D_Fit;

X_v_ALL = [X_v_ALL X_v];
O18_ALL = [O18_ALL O18];
D_ALL = [D_ALL D];
O18_Fit_ALL = [O18_Fit_ALL O18_Fit];
D_Fit_ALL = [D_Fit_ALL D_Fit];
O18_Residual_ALL = [O18_Residual_ALL O18_Residual];
D_Residual_ALL = [D_Residual_ALL D_Residual];

%% Plot
figure(1);
%%% H2O_ppm vs. O18_del %%%
subplot(2,2,1);
plot(X_v_ALL,O18_ALL,'.k');
hold on;
plot(X_v_ALL,O18_Fit_ALL,'.r');
hold off;
xlabel('\chi_v (ppm)');
ylabel('\delta^{18}O (^o/_{oo})');

subplot(2,2,3);
plot(X_v_ALL,O18_Residual_ALL,'.k');
xlabel('\chi_v (ppm)');
ylabel('\delta^{18}O (^o/_{oo})');

subplot(2,2,2);
plot(X_v_ALL,D_ALL,'.k');
hold on;
plot(X_v_ALL,D_Fit_ALL,'.r');
hold off;
xlabel('\chi_v (ppm)');
ylabel('\delta^{2}H (^o/_{oo})');

subplot(2,2,4);
plot(X_v_ALL,D_Residual_ALL,'.k');
xlabel('\chi_v (ppm)');
ylabel('\delta^{2}H (^o/_{oo})');

%% Output
csvwrite('CalibrationRMSE.csv',[X_v_ALL' O18_ALL' D_ALL' O18_Fit_ALL' D_Fit_ALL' O18_Residual_ALL' D_Residual_ALL']);

%% Bin and calculate RMSE
N = 184;
Y = quantile(X_v_ALL,N);
Y = [min(X_v_ALL) Y (max(X_v_ALL)+1)];
N_RMSE = NaN*ones(1,N+1);
X_v_RMSE = NaN*ones(1,N+1);
O18_RMSE = NaN*ones(1,N+1);
D_RMSE = NaN*ones(1,N+1);
for i = 1:N+1
   id = X_v_ALL >= Y(i) & X_v_ALL < Y(i+1);
   N_RMSE(i) = length(find(id));
   X_v_RMSE(i) = mean(X_v_ALL(id));
   O18_RMSE(i) = sqrt(mean(O18_Residual_ALL(id).^2));
   D_RMSE(i) = sqrt(mean(D_Residual_ALL(id).^2));
end

%% Curvefit RMSE
X_v_MODEL = 0:10:16000;

ft_O18 = fittype( 'exp2' );
[fitresult_O18, gof_O18] = fit(X_v_RMSE',O18_RMSE',ft_O18);
O18_MODEL = feval(fitresult_O18,X_v_MODEL);

ft_D = fittype( 'power1' );
[fitresult_D, gof_D] = fit(X_v_RMSE',D_RMSE',ft_D);
D_MODEL = feval(fitresult_D,X_v_MODEL);

figure(2);
subplot(2,1,1);
plot(X_v_RMSE,O18_RMSE);
hold on;
plot(X_v_MODEL,O18_MODEL,'r');
hold off;
xlabel('\chi_v (ppm)');
ylabel('RMSE \delta^{18}O (^o/_{oo} VSMOW)');
axis([0 16000 0 8]);
text(16000*.95,8*.95,sprintf('equation = %s\na = %f\nb = %f\nc = %f\nd = %f\nR^2 = %f',formula(fitresult_O18),fitresult_O18.a,fitresult_O18.b,fitresult_O18.c,fitresult_O18.d,gof_O18.rsquare),'VerticalAlignment','top','HorizontalAlignment','right');

subplot(2,1,2);
plot(X_v_RMSE,D_RMSE);
hold on;
plot(X_v_MODEL,D_MODEL,'r');
hold off;
xlabel('\chi_v (ppm)');
ylabel('RMSE \delta^{2}H (^o/_{oo} VSMOW)');
axis([0 16000 0 60]);
text(16000*.95,60*.95,sprintf('equation = %s\na = %f\nb = %f\nR^2 = %f',formula(fitresult_D),fitresult_D.a,fitresult_D.b,gof_D.rsquare),'VerticalAlignment','top','HorizontalAlignment','right');

