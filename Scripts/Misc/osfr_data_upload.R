#Following these instructions to authenticate {osfr}:
# https://docs.ropensci.org/osfr/articles/auth
#Specifically, I created an access token, and placed it in my Project's
# .Renviron file. Added .Renviron to .gitignore.

#Libraries
library(osfr)

#Need to refence a specific component in a project.
project_resources_data <- "https://osf.io/b3ckj/"
osf_data_proj <- osf_retrieve_node(project_resources_data)

#Grab the data archive
#This is a temporary local archive I made using 7zip, but have deleted to save
# space on my computer.
data_list <- list.files("Archive", "*.zip", full.names = TRUE)

#Upload!
#Commenting the code to prevent accidentally re-uploading data. 
# osf_upload(osf_data_proj, data_list, conflicts = "overwrite", progress = TRUE,
#   verbose = TRUE)
