snowy_raw <- raster::raster(tif_loc)
#Load the environmental data
load("../Data/Edit_Script/007_GLEESEnv.RData") #allEnv
#Convert to lat/lon
sites_ll <- sites %>%
sf::st_transform('+proj=longlat +datum=WGS84')
#Dynamic color generation that is loosely based on the colors in the manuscript
cp <- leaflet::colorFactor(c("#c04c27", "#0582cb"),
domain = c("Atmospheric Vapor", "Precipitation"))
#Plot!
leaflet::leaflet(sites_ll) %>%
leaflet::addProviderTiles(leaflet::providers$Esri.WorldTopoMap) %>%
leaflet::addCircleMarkers(radius = 10,
color = ~ cp(Type),
stroke = FALSE,
fillOpacity = 1,
popup = ~ Type)
common_crs <- raster::crs(snowy_raw)
#Clip the raster, because we don't need all of it.
# These are just eyeball estimates.
clip_box_df <- tibble(place = c("upper-left", "upper-right", "lower-left",
"lower-right"),
x = c(-106.356531, -106.097495, -106.356531, -106.097495),
y = c(41.415525, 41.415525, 41.275257, 41.275257))
clip_box <- clip_box_df
sp::coordinates(clip_box) <- ~ x + y
sp::proj4string(clip_box) <- as.character(common_crs)
snowy <- raster::crop(snowy_raw, clip_box)
plot(snowy)
?disaggregate
snowy_lowres <- disaggregate(snowy, fact = 10)
plot(snowy_lowres)
snowy_lowres <- aggregate(snowy, fact = 10)
plot(snowy_lowres)
################################################################################
#Convert the Snowy Range raster to a matrix.
snowy_mat = matrix(
data = raster::extract(snowy_lowres, raster::extent(snowy_lowres)),
nrow = ncol(snowy_lowres),
ncol = nrow(snowy_lowres))
#Convert the sites information to a dataframe with x, y information.
sites_df <- sites_ll %>%
as_tibble() %>%
unnest(geometry) %>%
mutate(coord = rep(c("lon", "lat"), times = nrow(.) / 2),
Id = rep(1:(nrow(.) / 2), each = 2)) %>%
pivot_wider(names_from = coord, values_from = geometry) %>%
mutate(color = cp(Type)) %>%
bind_cols(geoviz::latlong_to_rayshader_coords(snowy, .$lat, .$lon)) %>%
#geoviz makes a backward calculation
mutate(y = nrow(snowy) - y,
z = ifelse(Type == "Precipitation", 700, 1100))
################################################################################
#Convert the Snowy Range raster to a matrix.
snowy_mat = matrix(
data = raster::extract(snowy_lowres, raster::extent(snowy_lowres)),
nrow = ncol(snowy_lowres),
ncol = nrow(snowy_lowres))
#Convert the sites information to a dataframe with x, y information.
sites_df <- sites_ll %>%
as_tibble() %>%
unnest(geometry) %>%
mutate(coord = rep(c("lon", "lat"), times = nrow(.) / 2),
Id = rep(1:(nrow(.) / 2), each = 2)) %>%
pivot_wider(names_from = coord, values_from = geometry) %>%
mutate(color = cp(Type)) %>%
bind_cols(geoviz::latlong_to_rayshader_coords(snowy_lowres, .$lat, .$lon)) %>%
#geoviz makes a backward calculation
mutate(y = nrow(snowy_lowres) - y,
z = ifelse(Type == "Precipitation", 700, 1100))
#Borrowing from rayshader example, rather than exporting from package.
#I'm also changing the name so it makes more sense.
fliptb <- function(x) {
x[,ncol(x):1]
}
#Make the color overlay
tempfilename <- tempfile()
old.par <- par(no.readonly = TRUE)
on.exit(par(old.par))
png(tempfilename, width = nrow(snowy_mat), height = ncol(snowy_mat))
par(mar = c(0,0,0,0))
raster::image(fliptb(snowy_mat), axes = FALSE, col = terrain.colors(1000))
invisible(dev.off())
snowy_col <- png::readPNG(tempfilename)
#Initialize some of the rayshader inputs.
z_scale <- 8 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
z_scale <- 4 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
z_scale <- 20 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
z_scale <- 20 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
snowy_lowres <- aggregate(snowy, fact = 5)
plot(snowy_lowres)
################################################################################
#Convert the Snowy Range raster to a matrix.
snowy_mat = matrix(
data = raster::extract(snowy_lowres, raster::extent(snowy_lowres)),
nrow = ncol(snowy_lowres),
ncol = nrow(snowy_lowres))
#Convert the sites information to a dataframe with x, y information.
sites_df <- sites_ll %>%
as_tibble() %>%
unnest(geometry) %>%
mutate(coord = rep(c("lon", "lat"), times = nrow(.) / 2),
Id = rep(1:(nrow(.) / 2), each = 2)) %>%
pivot_wider(names_from = coord, values_from = geometry) %>%
mutate(color = cp(Type)) %>%
bind_cols(geoviz::latlong_to_rayshader_coords(snowy_lowres, .$lat, .$lon)) %>%
#geoviz makes a backward calculation
mutate(y = nrow(snowy_lowres) - y,
z = ifelse(Type == "Precipitation", 700, 1100))
tempfilename <- tempfile()
old.par <- par(no.readonly = TRUE)
on.exit(par(old.par))
png(tempfilename, width = nrow(snowy_mat), height = ncol(snowy_mat))
par(mar = c(0,0,0,0))
raster::image(fliptb(snowy_mat), axes = FALSE, col = terrain.colors(1000))
invisible(dev.off())
snowy_col <- png::readPNG(tempfilename)
#Initialize some of the rayshader inputs.
z_scale <- 20 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
#Add the points
sites_df %>%
split(.$Id) %>%
walk(~ rayshader::render_label(snowy_mat,
x = .x$x,
y = .x$y,
textcolor = .x$color,
linecolor = .x$color,
z = .x$z,
zscale = z_scale,
relativez = TRUE,
text = .x$Type,
freetype = FALSE))
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
snowy_lowres <- aggregate(snowy, fact = 4)
plot(snowy_lowres)
################################################################################
#Convert the Snowy Range raster to a matrix.
snowy_mat = matrix(
data = raster::extract(snowy_lowres, raster::extent(snowy_lowres)),
nrow = ncol(snowy_lowres),
ncol = nrow(snowy_lowres))
#Convert the sites information to a dataframe with x, y information.
sites_df <- sites_ll %>%
as_tibble() %>%
unnest(geometry) %>%
mutate(coord = rep(c("lon", "lat"), times = nrow(.) / 2),
Id = rep(1:(nrow(.) / 2), each = 2)) %>%
pivot_wider(names_from = coord, values_from = geometry) %>%
mutate(color = cp(Type)) %>%
bind_cols(geoviz::latlong_to_rayshader_coords(snowy_lowres, .$lat, .$lon)) %>%
#geoviz makes a backward calculation
mutate(y = nrow(snowy_lowres) - y,
z = ifelse(Type == "Precipitation", 700, 1100))
#Borrowing from rayshader example, rather than exporting from package.
#I'm also changing the name so it makes more sense.
fliptb <- function(x) {
x[,ncol(x):1]
}
#Make the color overlay
tempfilename <- tempfile()
old.par <- par(no.readonly = TRUE)
on.exit(par(old.par))
png(tempfilename, width = nrow(snowy_mat), height = ncol(snowy_mat))
par(mar = c(0,0,0,0))
raster::image(fliptb(snowy_mat), axes = FALSE, col = terrain.colors(1000))
invisible(dev.off())
snowy_col <- png::readPNG(tempfilename)
#Initialize some of the rayshader inputs.
z_scale <- 20 #Stretching out the elevations for a bit of visual appeal.
snowy_norm <- rayshader::calculate_normal(snowy_mat, zscale = z_scale)
snowy_shadow <- rayshader::ray_shade(snowy_mat, zscale = z_scale)
snowy_amb <- rayshader::ambient_shade(snowy_mat, zscale = z_scale)
#Visualize!
snowy_mat %>%
rayshader::sphere_shade(zscale = z_scale,
normalvectors = snowy_norm) %>%
rayshader::add_overlay(snowy_col, alphalayer = 0.6) %>%
rayshader::add_shadow(snowy_shadow) %>%
rayshader::add_shadow(snowy_amb) %>%
rayshader::plot_3d(snowy_mat,
zscale = z_scale,
zoom = 0.9,
baseshape = "rectangle")
rmarkdown::render_site()
install.packages("htmlTable")
rmarkdown::render_site()
warnings()
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(broom)
library(rstan)
#All the isotope data, by scenario
load("../Data/Edit_Script/009_AllIsotopeScenarios.RData") #allIso
#The results from a single scenario
file_loc <- paste("../Data/Edit_Script/ExpandedScenarios/",
"H2_Daily_annual_Method 1_unweighted_4_Air.rds",
sep = "/")
bias_example_raw <- read_rds(file_loc)
#Data manipulation
#Extract the measured data
bias_data <- allIso %>%
filter(isotope == "H2",
scale == "Daily",
season == "annual",
method == "Method 1",
weight == "unweighted",
lag == 4,
Temperature == "Air") %>%
select(date, Vapor.ppm, vapor, equil) %>%
drop_na() %>%
mutate(sample_id = 1:n())
#Extract the modeled data
bias_sample <- bias_example_raw %>%
as.data.frame(pars = c("atmos_true", "pred")) %>%
rownames_to_column(var = "iteration") %>%
pivot_longer(cols = -iteration, names_to = "parameter", values_to = "sample") %>%
mutate(sample_id = parse_number(parameter),
parameter = str_replace(parameter, "\\[[[:digit:]]*\\]", "")) %>%
pivot_wider(id_cols = c("iteration", "sample_id"), names_from = parameter,
values_from = sample) %>%
full_join(bias_data)
#Combine the data
bias_example <- bias_sample %>%
group_by(sample_id) %>%
summarise(vapor = mean(vapor),
Vapor.ppm = mean(Vapor.ppm),
lo = quantile(atmos_true, probs = 0.025),
mid = quantile(atmos_true, probs = 0.5),
hi = quantile(atmos_true, probs = 0.975))
bias_example %>%
lm(vapor ~ mid, data = .) %>%
summary()
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 2)) %>%
mutate(formula = pmap(., paste, sep = " + ")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured = ", formula, " * Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif") +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(\u2030)"),
y = expression(Measured~vapor*","~delta^2*H~"(\u2030)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
rmarkdown::render_site()
knitr::opts_chunk$set(echo = TRUE,
out.width = '100%')
library(tidyverse)
library(broom)
library(rstan)
#All the isotope data, by scenario
load("../Data/Edit_Script/009_AllIsotopeScenarios.RData") #allIso
#The results from a single scenario
file_loc <- paste("../Data/Edit_Script/ExpandedScenarios/",
"H2_Daily_annual_Method 1_unweighted_4_Air.rds",
sep = "/")
bias_example_raw <- read_rds(file_loc)
#Data manipulation
#Extract the measured data
bias_data <- allIso %>%
filter(isotope == "H2",
scale == "Daily",
season == "annual",
method == "Method 1",
weight == "unweighted",
lag == 4,
Temperature == "Air") %>%
select(date, Vapor.ppm, vapor, equil) %>%
drop_na() %>%
mutate(sample_id = 1:n())
#Extract the modeled data
bias_sample <- bias_example_raw %>%
as.data.frame(pars = c("atmos_true", "pred")) %>%
rownames_to_column(var = "iteration") %>%
pivot_longer(cols = -iteration, names_to = "parameter", values_to = "sample") %>%
mutate(sample_id = parse_number(parameter),
parameter = str_replace(parameter, "\\[[[:digit:]]*\\]", "")) %>%
pivot_wider(id_cols = c("iteration", "sample_id"), names_from = parameter,
values_from = sample) %>%
full_join(bias_data)
#Combine the data
bias_example <- bias_sample %>%
group_by(sample_id) %>%
summarise(vapor = mean(vapor),
Vapor.ppm = mean(Vapor.ppm),
lo = quantile(atmos_true, probs = 0.025),
mid = quantile(atmos_true, probs = 0.5),
hi = quantile(atmos_true, probs = 0.975))
bias_example %>%
lm(vapor ~ mid, data = .) %>%
summary()
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 2)) %>%
mutate(formula = pmap(., paste, sep = " + ")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured = ", formula, " %.% Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif") +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(‰)"),
y = expression(Measured~vapor*","~delta^2*H~"(‰)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 2)) %>%
mutate(formula = pmap(., paste, sep = "~+~")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured~=~", formula, "~%.%~Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif", parse = TRUE) +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(‰)"),
y = expression(Measured~vapor*","~delta^2*H~"(‰)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 2)) %>%
mutate(formula = pmap(., paste, sep = "~+~")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured=", formula, "%.%Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif", parse = TRUE) +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(‰)"),
y = expression(Measured~vapor*","~delta^2*H~"(‰)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 3)) %>%
mutate(formula = pmap(., paste, sep = "~+~")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured=", formula, "%.%Predicted"))
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 3)) %>%
mutate(formula = pmap(., paste, sep = "~+~")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured*=*", formula, "%.%Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif", parse = TRUE) +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(‰)"),
y = expression(Measured~vapor*","~delta^2*H~"(‰)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
bias_lm <- bias_example %>%
lm(vapor ~ mid, data = .) %>%
tidy() %>%
select(term, estimate) %>%
pivot_wider(names_from = term, values_from = estimate) %>%
mutate_all(~ round(., 3)) %>%
mutate(formula = pmap(., paste, sep = "~+~")) %>%
unnest(formula) %>%
mutate(formula = paste0("Measured~'='~", formula, "%.%Predicted"))
bias_example %>%
ggplot(aes(mid, vapor, fill = Vapor.ppm)) +
geom_abline(intercept = 0, slope = 1) +
geom_smooth(method = "lm", se = FALSE, linetype = "dashed", color = "red") +
geom_errorbar(aes(ymin = lo, ymax = hi)) +
geom_point(shape = 21, size = 4, alpha = 0.7) +
geom_text(data = bias_lm, aes(label = formula), inherit.aes = FALSE,
x = -295, y = -125, hjust = 0, family = "serif", parse = TRUE) +
labs(x = expression(Predicted~vapor*","~delta^2*H~"(‰)"),
y = expression(Measured~vapor*","~delta^2*H~"(‰)"),
fill = "Humidity\n(ppm)") +
theme_bw() +
theme(text = element_text(family = "serif"),
panel.grid.minor = element_blank()) +
scale_fill_distiller(palette = "Spectral")
