# About the website

Just a very brief introduction to the purpose of the website.
* Link to the publication
* Purpose of the paper
* Link to compendium
	* Information about the compendium: code and data
* Link to the pre-print
	* Overleaf
	
* Figure: Concpetual model of the controls of vapor isotopes.

Provide a brief description of the various sections and subsections for the rest of the site, with links.

Links: OSF compendium.

I also need to include a link to my blog

# Introduction

## Introduction: Purpose & Objectives

A very quick blurb about the motivations for the study (and maybe why I used bayesian analysis, with reference to that section)

# Methods

## Methods: Site & Environmental Conditions

* Link to the OSF compendium with the complete data set.
* OSF compendium has all the data.

Mention that the data can be downloaded from individual sections

### Geographic location

* Interactive figure: Site locations.
* Updated figure: Site locations using rayshader.

* Download button: Site location information.

### Environmental conditions

* Update figure: hyetograph of precipitation

* Download button: Environmental conditions at GLEES.

## Methods: Data collection and analysis

### Basic isotope theory

This will need to be extremely brief. Maybe provide some textbook references.

### Isotope measurements

* Figure: Time series of both the vapor and precipitation data, by isotope

* Download button: Aggregate vapor isotope data.
* Download button: Precipitation isotope data.

### About bayesian statistics

* Figure/animation: Evolution of coefficient estimates for a set of covariates and a single chain.
* Figure/animation: Evolution of multiple chains relative to the predicted values of the posterior and sigma.

# Results

## Isotopic data

### Equilibrium Scenarios

* Figures: The various scenarios that performed better than the average
   * I need to update this figure so it is more discernable as to what the facet labels mean.
* Dynamic table: Posteriors of the global error
   * Could I make clickable links providing a pop-up of visualization of each posterior?

### Apparent Disequilibrium Scenarios

* Dynamic table: Posteriors of the global error and apparent disequilibrium

### Environmental Covariates

* Dynamic table: Posteriors of the various covariate coefficients and global error

# Discussions and conclusion

* Figure: Conceptual figure

## Implications for understanding mountain lake hydrology

Make a simple lake mass balance system users can play with to better understand how uncertainties in vapor impact our understanding of mountain lake processes. Keep it simple:
* Closed basin (only need to worry about precipitation, evaporation, groundwater input, groundwater output)
* Process rates are constant
* Small volume

Could I use a flex dashboard for this page?

# Supplemental information

## Notation

Include links to download a pdf version of the definitions and relationships. I can make some Rmd files that output as pdfs (copy from Overleaf).

### Definitions

### Mathematical Relationships

## Measurement Uncertainty







