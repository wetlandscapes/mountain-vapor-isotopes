Welcome to the repository for the manuscript _Atmospheric vapor and precipitation are not in isotopic equilibrium in a continental mountain environment_.

![Conceptual diagram](./public/static/figures/UpdatedUnderstandingOfProcessesInfluencingVaporIsotopeValues.svg)*Figure: A comparison of conceptual models related to the processes likely influencing atmospheric vapor isotope concentrations.*

Here you will find the underlying code used in the manuscript (with some version history), as well as the code used for the supplemental website for the manuscript.

The supplemental website is located at: https://wetlandscapes.gitlab.io/mountain-vapor-isotopes/

Code for the website is located in the `public` folder above.

Code related to the analysis in the manuscript is located in the `Functions` and `Scripts` folders. For more information about the code, as well as access to data and other information (e.g. a pre-print of the paper), please visit the compendium associated with the manuscript, hosted at the Open Science Framework: https://osf.io/7he4x/.


